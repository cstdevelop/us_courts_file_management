<?php
/**
 * @file
 * us_courts_file_management.features.content_menu_links.inc
 */

/**
 * Implements hook_content_menu_links_defaults().
 */
function us_courts_file_management_content_menu_links_defaults() {
  $menu_links = array();

  // Exported menu link: management:media
  $menu_links['management:media'] = array(
    'menu_name' => 'management',
    'link_path' => 'media',
    'router_path' => 'media',
    'link_title' => 'File browser',
    'options' => array(
      'attributes' => array(
        'title' => 'Navigation link to file browser',
      ),
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -14,
    'customized' => 0,
    'parent_path' => 'admin',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('File browser');


  return $menu_links;
}
